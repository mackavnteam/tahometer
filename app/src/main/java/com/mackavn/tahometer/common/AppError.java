package com.mackavn.tahometer.common;


/**
 * Created by llaerto
 * on 31.01.2017,
 * using dell
 */

public class AppError {
    private String code;
    private String message;
    private Throwable throwable;

    public AppError() {

    }

    public AppError(Throwable throwable) {
        this.throwable = throwable;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        if (message == null) {
            if (throwable != null) {
                message = throwable.getLocalizedMessage();
            }
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

}
