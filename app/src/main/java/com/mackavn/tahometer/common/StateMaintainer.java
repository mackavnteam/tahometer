package com.mackavn.tahometer.common;

import java.util.HashMap;

public class StateMaintainer {

    private HashMap<String, Object> data;
    private static StateMaintainer instance;

    public StateMaintainer() {
        this.data = new HashMap<>();
        instance = this;
    }

    public boolean firstTimeIn(android.support.v4.app.FragmentManager fragmentManager, String fragmentTag) {
        try {
            android.support.v4.app.Fragment stateMaintainerFrag = fragmentManager.findFragmentByTag(fragmentTag);
            if (stateMaintainerFrag == null) {
                stateMaintainerFrag = new android.support.v4.app.Fragment();
                fragmentManager.beginTransaction()
                        .add(stateMaintainerFrag, fragmentTag).commit();
                return true;
            }
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public static StateMaintainer getInstance() {
        return instance;
    }

    public void put(String key, Object obj) {
        data.put(key, obj);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) data.get(key);
    }
}
