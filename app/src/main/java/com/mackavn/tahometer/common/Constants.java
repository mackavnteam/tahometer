package com.mackavn.tahometer.common;

import android.Manifest;

public final class Constants {
    public static final String APP_TAG = "App";
    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";
    //network
    public static final String SERVER_URL = "http://google.com";
    // permissions
    public static final String[] REQUIRED_PERMISSIONS_STORAGE = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    //time constants
    public static final int ONE_SECOND = 1000;
    public static final int TEN_SECONDS = ONE_SECOND * 10;
    public static final int ONE_MINUTE = 60 * ONE_SECOND;
    public static final int ONE_HOUR = 60 * ONE_MINUTE;
    //timeouts
    public static final int DEFAULT_SERVER_READ_TIMEOUT = 15;
    public static final int DEFAULT_SERVER_CONNECT_TIMEOUT = 15;

}
