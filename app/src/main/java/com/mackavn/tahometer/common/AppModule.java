package com.mackavn.tahometer.common;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mackavn.tahometer.App;
import com.mackavn.tahometer.bus.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mykhailo Diachenko
 * on 26.12.16.
 */

@Module
public class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    App provideApp() {
        return app;
    }

    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }
}
