package com.mackavn.tahometer.interfaces.model;

import com.mackavn.tahometer.di.AppComponent;

public interface BaseModelActions {
    /* actions of model logic, for example - load data from server */
    void onDestroy();
    AppComponent injector();
}
