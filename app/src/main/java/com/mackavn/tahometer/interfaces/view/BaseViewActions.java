package com.mackavn.tahometer.interfaces.view;

import android.content.Context;
import android.support.annotation.Nullable;

import com.mackavn.tahometer.common.AppError;
import com.mackavn.tahometer.di.AppComponent;

public interface BaseViewActions {
    /* actions of model logic, for example - refresh ui */ Context getContext();
    AppComponent injector();
    void showError(@Nullable AppError error);
    void showProgressBar();
    void showProgressView();
    void showEmptyView();
    void lockUi();
    boolean isUiLocked();
    void unlockUi();
    void hideKeyboard();
}
