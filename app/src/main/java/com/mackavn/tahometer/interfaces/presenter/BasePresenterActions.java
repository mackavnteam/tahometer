package com.mackavn.tahometer.interfaces.presenter;

import android.support.annotation.Nullable;

import com.mackavn.tahometer.common.AppError;
import com.mackavn.tahometer.di.AppComponent;

public interface BasePresenterActions {
    /* presenter actions - should communicate model with view through presenter */

    /* View -> Presenter actions
     * actions should communicate from view to presenter. Presenter should do something
     * and decide next step if it is necessary
     */
    interface ViewActions {
        void onViewCreated();
        void onViewDestroyed();
        void onDestroy();
    }

    /* Model -> Presenter
     * actions should communicate from model to presenter. Presenter should do somenthing
     * and decide next step if it is necessary
     */
    interface ModelActions {
        void onError(@Nullable AppError appError);
        AppComponent injector();
    }
}
