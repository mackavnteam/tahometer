package com.mackavn.tahometer.model;

import com.mackavn.tahometer.common.AppError;
import com.mackavn.tahometer.di.AppComponent;
import com.mackavn.tahometer.interfaces.model.BaseModelActions;
import com.mackavn.tahometer.interfaces.presenter.BasePresenterActions;

import rx.subscriptions.CompositeSubscription;

public class BaseModel<T extends BasePresenterActions.ModelActions>
        implements BaseModelActions {

    private T presenter;
    private CompositeSubscription compositeSubscription;

    protected BaseModel(T presenter) {
        this.presenter = presenter;
        compositeSubscription = new CompositeSubscription();
    }

    protected T getPresenter() { return presenter; }

    protected CompositeSubscription getCompositeSubscription() {
        return compositeSubscription;
    }

    /* Model operations: presenter -> model */

    @Override
    public void onDestroy() {
        presenter = null;
        compositeSubscription.clear();
    }

    @Override
    public AppComponent injector() {
        return presenter != null ? presenter.injector() : null;
    }

    /* Inner methods */

    protected void onServerLoadError(Throwable throwable) {
        if (getPresenter() != null) {
            AppError error = new AppError();
            error.setThrowable(throwable);
            getPresenter().onError(error);
        }
    }
}

