package com.mackavn.tahometer.presenter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.mackavn.tahometer.common.AppError;
import com.mackavn.tahometer.di.AppComponent;
import com.mackavn.tahometer.interfaces.model.BaseModelActions;
import com.mackavn.tahometer.interfaces.presenter.BasePresenterActions;
import com.mackavn.tahometer.interfaces.view.BaseViewActions;

import java.lang.ref.WeakReference;

import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<T extends BaseViewActions, M extends BaseModelActions>
        implements BasePresenterActions.ViewActions, BasePresenterActions.ModelActions {

    private WeakReference<T> view;
    private M modelController;
    private boolean isRunning;
    private CompositeSubscription compositeSubscription;

    @SuppressWarnings("all")
    protected BasePresenter(T view) {
        isRunning = true;
        this.view = new WeakReference<>(view);
        modelController = createModelInstance();
        compositeSubscription = new CompositeSubscription();
        subscribeOnEvents();
    }

    public void setView(T view) {
        isRunning = true;
        modelController = createModelInstance();
        compositeSubscription = new CompositeSubscription();
        this.view = new WeakReference<>(view);
        subscribeOnEvents();
    }

    /* Inner methods */

    protected abstract M createModelInstance();

    protected T getView() {
        if (isRunning && view != null) {
            return view.get();
        } else {
            return null;
        }
    }

    protected M getModel() {
        return modelController;
    }

    protected CompositeSubscription getCompositeSubscription() {
        return compositeSubscription;
    }

    protected Context getContext() {
        if (view != null) {
            return view.get().getContext();
        } else {
            return null;
        }
    }

    @SuppressWarnings("all")
    protected void subscribeOnEvents() {
        /* here should be subscription to events*/
    }

    /* View calls */

    public void onViewDestroyed() {
        compositeSubscription.clear();
    }

    public void onDestroy() {
        view = null;
        isRunning = false;
        modelController.onDestroy();
    }

    @Override
    public AppComponent injector() {
        return getView() != null ? getView().injector() : null;
    }

    /* Model callbacks */

    public void onError(@Nullable AppError error) {
        if (getView() != null) {
            getView().unlockUi();
        }
        if (error != null && getView() != null) {
            getView().showError(error);
        }
    }
}
