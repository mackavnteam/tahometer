package com.mackavn.tahometer;

import android.app.Application;

import com.mackavn.tahometer.common.AppModule;
import com.mackavn.tahometer.common.Constants;
import com.mackavn.tahometer.di.AppComponent;
import com.mackavn.tahometer.di.DaggerAppComponent;
import com.mackavn.tahometer.network.NetModule;

import io.realm.Realm;

/**
 * Created by Mykhailo Diachenko
 * on 26.12.16.
 */

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.SERVER_URL))
                .build();

        Realm.init(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
