package com.mackavn.tahometer.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtils {
    public static final String ISO_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    private static final SimpleDateFormat SDF_DATE_TIME = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

    public static String getCurrentTimeString() {
        Calendar today = Calendar.getInstance();
        return formatAndroidTime(today);
    }

    public static String format(Calendar date) {
        return SDF_DATE_TIME.format(date.getTime());
    }

    public static Calendar parse(String s) {
        try {
            Date date = SDF_DATE_TIME.parse(s);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException e) {
            return null;
        }
    }

    private static String formatAndroidTime(Calendar date) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        timeFormat.setCalendar(date);
        return timeFormat.format(date.getTime());
    }
}
