package com.mackavn.tahometer.utils;

import android.widget.TextView;

import javax.inject.Inject;

public class ValidateUtils {

    @Inject
    public ValidateUtils() {

    }

    public String nullToString(String nullToString) {
        return nullToString != null ? nullToString : "";
    }

    public boolean validMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(nullToString(email)).matches();
    }

    public String tvtoString(TextView view) {
        return view != null ? nullToString(view.getText().toString()) : "";
    }
}
