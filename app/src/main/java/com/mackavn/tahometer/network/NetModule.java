package com.mackavn.tahometer.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mackavn.tahometer.common.Constants;
import com.mackavn.tahometer.network.api.ExampleApiManager;
import com.mackavn.tahometer.network.data.response.ResponseDeserializer;
import com.mackavn.tahometer.network.data.response.container.ResponseData;
import com.mackavn.tahometer.utils.DateTimeUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mykhailo Diachenko
 * on 26.12.16.
 */

@Module
public class NetModule {
    String baseUrl;

    // Constructor needs one parameter to instantiate.
    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    // Dagger will only look for methods annotated with @Provides

    @Provides
    @Singleton
    Gson provideGson(ResponseDeserializer responseDeserializer) {
        return new GsonBuilder()
                .setDateFormat(DateTimeUtils.ISO_TIME_FORMAT)
                .registerTypeAdapter(ResponseData.class, responseDeserializer)
                .create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(Constants.DEFAULT_SERVER_READ_TIMEOUT, TimeUnit.SECONDS);
        builder.connectTimeout(Constants.DEFAULT_SERVER_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        builder.addInterceptor(chain -> {
            Request request = chain.request();
            Log.d(Constants.APP_TAG, request.url().toString());
            return chain.proceed(request);
        });
        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    ExampleApiManager provideApiManager(Retrofit retrofit) {
        return ExampleApiManager.newInstance(retrofit);
    }
}
