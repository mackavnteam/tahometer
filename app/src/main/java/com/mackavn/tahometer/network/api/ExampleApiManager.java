package com.mackavn.tahometer.network.api;

import com.mackavn.tahometer.network.api.retrofit.ApiInterface;

import retrofit2.Retrofit;

/**
 * Created by Mykhailo Diachenko
 * on 26.12.16.
 */

public class ExampleApiManager extends BaseApiManager<ApiInterface> {
    //rename this class according to ApiInterface.
    public static ExampleApiManager newInstance(Retrofit retrofit) {
        return new ExampleApiManager(retrofit);
    }

    private ExampleApiManager(Retrofit retrofit) {
        super(retrofit, ApiInterface.class);
    }

    //below should be api calls
}
