package com.mackavn.tahometer.network.data.response.container;

import com.google.gson.annotations.SerializedName;

public class ResponseData<T> {

    @SerializedName(value = "result")
    private T data;

    public ResponseData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public boolean isSuccessful() {
        return data != null;
    }
}
