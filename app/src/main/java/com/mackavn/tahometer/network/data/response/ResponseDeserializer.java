package com.mackavn.tahometer.network.data.response;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.mackavn.tahometer.network.data.response.container.ResponseData;

import java.lang.reflect.Type;

import javax.inject.Inject;

public class ResponseDeserializer implements JsonDeserializer<ResponseData> {

    @Inject
    public ResponseDeserializer() {

    }

    @Override
    public ResponseData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        return new Gson().fromJson(jsonObject, typeOfT);
    }
}
