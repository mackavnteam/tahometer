package com.mackavn.tahometer.di;

import com.mackavn.tahometer.common.AppModule;
import com.mackavn.tahometer.network.NetModule;
import com.mackavn.tahometer.view.activities.StartActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Mykhailo Diachenko
 * on 26.12.16.
 */

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface AppComponent {
    void inject(StartActivity activity);
}
