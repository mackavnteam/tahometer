package com.mackavn.tahometer.view.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mackavn.tahometer.App;
import com.mackavn.tahometer.R;
import com.mackavn.tahometer.bus.RxBus;
import com.mackavn.tahometer.network.api.ExampleApiManager;

import javax.inject.Inject;


/**
 * Created by Mykhailo Diachenko
 * on 26.12.16.
 */

public class StartActivity extends BaseActivity {
    @Inject
    ExampleApiManager apiManager;
    @Inject
    RxBus rxBus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().inject(this);
        Log.d("TAG", "netModule injection " + apiManager);
        Log.d("TAG", "appModule injection " + rxBus);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_single_fragment;
    }

    @Override
    protected void initView() {

    }
}
