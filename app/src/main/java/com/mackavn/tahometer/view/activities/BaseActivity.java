package com.mackavn.tahometer.view.activities;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.mackavn.tahometer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    @Nullable
    protected Toolbar toolbar;
    @BindView(R.id.coordinator)
    @Nullable
    protected CoordinatorLayout coordinator;

    protected ActionBar actionBar;
    protected int currentOrientation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
        }
        currentOrientation = getResources().getConfiguration().orientation;
        initView();
        setWhiteToolbar();
    }

    public void setToolbarTitle(String res) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(res);
        }
    }

    public void setToolbarTitle(@StringRes int res) {
        setToolbarTitle(getString(res));
    }

    //this method will correct application toolbar title color on opened apps stack
    //use it if your toolbar text color white, but toolbar is one of light colors

    private void setWhiteToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int color = ContextCompat.getColor(this, R.color.cardview_dark_background);
            ActivityManager.TaskDescription td = new ActivityManager.TaskDescription(null, null, color);
            setTaskDescription(td);
        }
    }

    /* abstract */

    protected abstract int getLayoutResId();

    protected abstract void initView();

    /* Display snacks & toasts */

    protected void shortToast(@StringRes int id) {
        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
    }

    protected void longToast(@StringRes int id) {
        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();
    }

    protected void shortToast(@NonNull String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    protected void longToast(@NonNull String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    protected void shortSnack(@StringRes int id) {
        if (coordinator != null) {
            Snackbar.make(coordinator, id, Snackbar.LENGTH_SHORT).show();
        }
    }

    protected void shortSnack(@NonNull String val) {
        if (coordinator != null) {
            Snackbar.make(coordinator, val, Snackbar.LENGTH_SHORT).show();
        }
    }

    protected void longSnack(@StringRes int id) {
        if (coordinator != null) {
            Snackbar.make(coordinator, id, Snackbar.LENGTH_LONG).show();
        }
    }

    protected void longSnack(@NonNull String val) {
        if (coordinator != null) {
            Snackbar.make(coordinator, val, Snackbar.LENGTH_LONG).show();
        }
    }

    protected void actionSnack(@StringRes int id, @StringRes int but, View.OnClickListener listener) {
        if (coordinator != null) {
            Snackbar.make(coordinator, id, Snackbar.LENGTH_INDEFINITE)
                    .setAction(but, listener)
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            if (event == DISMISS_EVENT_SWIPE) {
                                actionSnackRecursive(id, but, listener);
                            }
                        }
                    }).show();
        }
    }

    protected void actionSnackRecursive(@StringRes int id, @StringRes int but, View.OnClickListener listener) {
        if (coordinator != null) {
            Snackbar.make(coordinator, id, Snackbar.LENGTH_INDEFINITE)
                    .setAction(but, listener)
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            if (event == DISMISS_EVENT_SWIPE) {
                                actionSnackRecursive(id, but, listener);
                            }
                        }
                    }).show();
        }
    }

    protected void launchActivity(Class clzz) {
        startActivity(new Intent(this, clzz));
        overridePendingTransition(R.anim.fast_fade_in, R.anim.fast_fade_out);
    }

}
