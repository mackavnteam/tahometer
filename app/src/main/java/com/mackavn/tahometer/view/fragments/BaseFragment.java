package com.mackavn.tahometer.view.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import com.mackavn.tahometer.App;
import com.mackavn.tahometer.R;
import com.mackavn.tahometer.common.StateMaintainer;
import com.mackavn.tahometer.di.AppComponent;
import com.mackavn.tahometer.interfaces.presenter.BasePresenterActions;
import com.mackavn.tahometer.interfaces.view.BaseViewActions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment<T extends BasePresenterActions.ViewActions> extends Fragment implements
        BaseViewActions {

    @Nullable @BindView(R.id.progress_view) View progressView;
    @Nullable @BindView(R.id.empty_view) View emptyView;

    private T presenter;
    private StateMaintainer stateMaintainer;
    private Unbinder unbinder;
    private Animation fadeInAnimation, fadeOutAnimation;
    protected ProgressDialog progressDialog;
    protected boolean disabled;

    /* Superclass methods */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(isRetain());
        fadeOutAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fast_fade_out);
        fadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fast_fade_in);
        restore();
    }

    protected void restore() {
        if (isRestoreAble()) {
            stateMaintainer = StateMaintainer.getInstance();
            if (stateMaintainer == null) {
                stateMaintainer = new StateMaintainer();
            }
            try {
                if (stateMaintainer.firstTimeIn(getActivity().getSupportFragmentManager(), getTagName())) {
                    initialize();
                } else {
                    reinitialize();
                }
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                initialize();
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        }
    }

    protected boolean isRestoreAble() {
        return true;
    }

    protected boolean isRetain() {
        return true;
    }

    protected void resetView() {
        //do here something on reset
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutResId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            hideKeyboard();
        } catch (Exception e) {
            //nothing to do
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        presenter.onViewDestroyed();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        presenter = null;
        super.onDestroy();
    }

    public AppComponent injector() {
        return ((App) getActivity().getApplication()).getAppComponent();
    }

    protected T getPresenter() {
        return presenter;
    }

    @Override
    public void showProgressBar() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    public void showProgressView() {
        if (progressView != null) {
            progressView.setVisibility(View.VISIBLE);
        }
        if (emptyView != null) {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyView() {
        if (emptyView != null) {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void lockUi() {
        disabled = true;
    }

    @Override
    public boolean isUiLocked() {
        return disabled;
    }

    @Override
    public void unlockUi() {
        disabled = false;
        hideProgress();
    }

    @Override
    public void hideKeyboard() {
        if (getContext() != null && getView() != null) {
            InputMethodManager imm = (InputMethodManager) getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    /* Inner methods */

    protected void fadeAction(View view, boolean in) {
        view.setVisibility(in ? View.VISIBLE : View.GONE);
        view.startAnimation(in ? fadeInAnimation : fadeOutAnimation);
    }

    private void initialize()
            throws InstantiationException, IllegalAccessException {
        presenter = createPresenter();
        if (isRestoreAble()) {
            stateMaintainer.put(getTagName(), presenter);
        }
        initProgressDialog();
    }

    private void initProgressDialog() {
        if (getProgressTitle() == null && getProgressMessage() == null) {
            return;
        }
        progressDialog = new ProgressDialog(getContext());
        if (getProgressTitle() != null) {
            progressDialog.setTitle(getProgressTitle());
        }
        if (getProgressMessage() != null) {
            progressDialog.setMessage(getProgressMessage());
        }
    }

    private void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (progressView != null) {
            progressView.setVisibility(View.GONE);
        }
    }

    private void reinitialize()
            throws InstantiationException, IllegalAccessException {
        presenter = stateMaintainer.get(getTagName());
        if (presenter == null) {
            initialize();
        } else {
            resetView();
        }
    }

    protected String getProgressTitle() {
        return null;
    }

    protected String getProgressMessage() {
        return getString(R.string.progress_message);
    }

    /* Abstract methods */

    protected abstract int getLayoutResId();

    protected abstract T createPresenter();

    public abstract String getTagName();

}
