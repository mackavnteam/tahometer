package com.mackavn.tahometer.view.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mackavn.tahometer.common.Constants;
import com.mackavn.tahometer.utils.FontUtils;

public class CustomTextView extends TextView {

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public CustomTextView(Context context) {
        super(context);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        int textStyle = attrs.getAttributeIntValue(Constants.ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        Typeface customFont = selectTypeface(context, textStyle);
        setTypeface(customFont);
    }

    private Typeface selectTypeface(Context context, int textStyle) {
        switch (textStyle) {
            case Typeface.BOLD: // bold
                return FontUtils.getTypeface("fonts/mp-bold.ttf", context);

            case Typeface.ITALIC: // italic
                return FontUtils.getTypeface("fonts/mp-light.otf", context);

            case Typeface.BOLD_ITALIC: // bold italic
                return FontUtils.getTypeface("fonts/mp-bold-condensed.ttf", context);

            case Typeface.NORMAL: // regular
            default:
                return FontUtils.getTypeface("fonts/mp-regular.ttf", context);
        }
    }
}
